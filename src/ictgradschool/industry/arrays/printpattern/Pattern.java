package ictgradschool.industry.arrays.printpattern;

public class Pattern
    {
        private int number;
        private char printCharacter;
        public Pattern(int number, char printCharacter)
            {
            this.number = number;
            this.printCharacter = printCharacter;
            }
        public int getNumberOfCharacters()
            {
                return number;
            }
            public void setNumberOfCharacters(int newNumber)
                {
                    number = newNumber;
                }
    @Override
    public String toString()
        {
            String s = "";
            for(int i = 0; i<number; i++)
            {
                s += printCharacter;
            }
        return s;
        }
    }
